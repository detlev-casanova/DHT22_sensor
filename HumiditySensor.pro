TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c

LIBS += -lwiringPi

INSTALLS        = target
target.files    = HumiditySensor
target.path     = /root/
