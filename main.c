/*
 *	read and log temperature and humidity from DHT22 sensor
 */

#include <wiringPi.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

int data[5] = { 0, 0, 0, 0, 0 };

int waitForState(int pin, int state)
{
    int counter = 0;
    while ( digitalRead( pin ) != state )
    {
        delayMicroseconds( 1 );
        if ( counter++ == 255 )
        {
            printf("Sensor never pulled down\n");
            return -1;
        }
    }

    return 0;
}

int readDhtData(int pin, float *humidity, float *temperature)
{
    uint8_t counter;
    uint16_t i;

    data[0] = data[1] = data[2] = data[3] = data[4] = 0;

    /* pull pin down for 18 milliseconds */
    pinMode( pin, OUTPUT );
    digitalWrite( pin, LOW );
    delay( 5 );
    digitalWrite( pin, HIGH );

    /* prepare to read the pin */
    pinMode( pin, INPUT );

    /* detect change and read data */
    counter = 0;

    if (waitForState(pin, LOW) != 0)
        return -1;

    if (waitForState(pin, HIGH) != 0)
        return -1;

    // Ready for reading bits
    if (waitForState(pin, LOW) != 0)
        return -1;

    for (i = 0; i < 40; i++)
    {
        if (waitForState(pin, HIGH) != 0)
            return -1;

        // Count delay until low again
        counter = 0;
        while (1) {
            delayMicroseconds( 10 );
            if (digitalRead( pin ) == LOW)
                break;

            if (++counter > 20) {
                printf("PIN never went LOW\n");
                return -1;
            }
        }

        /* shove each bit into the storage bytes */
        data[i / 8] <<= 1;

        // 6*5 us = 30 us
        if ( counter > 3 )
            data[i / 8] |= 1;
    }

    if ( (data[4] == ( (data[0] + data[1] + data[2] + data[3]) & 0xFF) ) )
    {
        float h = (float)((data[0] << 8) + data[1]) / 10;
        float c = (float)(((data[2] & 0x7F) << 8) + data[3]) / 10;

        if ( data[2] & 0x80 )
            c = -c;

        *humidity = h;
        *temperature = c;
    }else  {
        printf( "Data not good, skip\n" );
        return -1;
    }

    return 0;
}

//TODO: log per day file
int logInFile(float value, const char * fileName)
{
    if (!fileName)
        return 0;

    struct timeval tp;

    gettimeofday(&tp, NULL);

    FILE *file = fopen(fileName, "a+");
    if (!file) {
        printf("Cannot open file %s : %s\n", fileName, strerror(errno));
        return 1;
    }

    if (fprintf(file, "%lu, %.1f\n", (unsigned long)tp.tv_sec, value) < 0) {
        printf("Cannot write to file %s\n", fileName);
        return 1;
    }

    fclose(file);

    return 0;
}

int main(int argc, char *argv[])
{
    int arg, pin = 3, ret = 0;
    float humidity, temperature;
    char *temperatureFile = NULL;
    char *humidityFile = NULL;

    printf( "Raspberry Pi DHT11/DHT22 temperature/humidity test\n" );

    if ( wiringPiSetup() == -1 )
        return 1;

    while((arg = getopt(argc, argv, "p:t:H:c:h")) != -1) {
        switch (arg) {
        case 'p':
            pin = atoi(optarg);
            break;
        case 't':
            temperatureFile = malloc(strlen(optarg) + 1);
            strcpy(temperatureFile, optarg);
            printf("Logging temperature in %s\n", temperatureFile);
            break;
        case 'H':
            humidityFile = malloc(strlen(optarg) + 1);
            strcpy(humidityFile, optarg);
            printf("Logging humidity in %s\n", humidityFile);
            break;
        case 'h':
            printf("%s: for help, read the code (-p to set wiringPi pin, -t for temperature file, -H for humidity file, -c for sample count)\n", argv[0]);
            break;
        }
    }

    while (1) {
        /*unsigned int toWait = 5;
        struct timespec sleeper ;
        struct timeval tBef, tAft;
        gettimeofday (&tBef, NULL) ;
        delayMicroseconds( toWait );
        gettimeofday (&tAft, NULL) ;
        printf("[wiringPi] before=%u, after=%u (waited %u us)\n", tBef.tv_usec, tAft.tv_usec, tAft.tv_usec - tBef.tv_usec);

        sleeper.tv_sec  = 0 ;
        sleeper.tv_nsec = (long)(toWait * 1000L) ;

        gettimeofday (&tBef, NULL) ;
        nanosleep (&sleeper, NULL) ;
        gettimeofday (&tAft, NULL) ;

        printf("[nanosleep] before=%u, after=%u (waited %u us)\n", tBef.tv_usec, tAft.tv_usec, tAft.tv_usec - tBef.tv_usec);*/

        if (readDhtData(pin, &humidity, &temperature) == 0) {
            if (!humidityFile && !temperatureFile) {
                printf("Humidity = %.1f %% Temperature = %.1f *C\n", humidity, temperature);
            } else if (logInFile(humidity, humidityFile) != 0 ||
                       logInFile(temperature, temperatureFile) != 0) {
                printf("An error occured when logging values, leaving\n");
                ret = 1;
                goto leave;
            }
        }

        delay( 5000 );
    }

leave:
    free(temperatureFile);
    free(humidityFile);

    return ret;
}
